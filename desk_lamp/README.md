# Overview

# BOM

- aluminum bar for arm (I used 1" x 1/8"--I had a piece that was about 575 mm long leftover from something)
  - drill holes to fit M5 screws to mount to extrusion
  - clean & sand & clean
- aluminum extrusion
  - (I used a piece of 20x60 that was 250 mm long from
  [OpenBuilds]((https://openbuildspartstore.com/v-slot-20x60-linear-rail/))
  - tap aluminum extrusion ends
- 3d printed base see 3d_printed_parts sub-folder
- rotary encoder from [Adafruit](https://www.adafruit.com/product/377)
- ATTiny85 microcontroller (I used an Uno to program it)
- 3.3 volt voltage regulator + capacitors for input and output
- 12 volt power supply (like from an old external hard-drive)
- Jack to plug in the power supply (base was designed to fit
  [this one](https://www.digikey.com/products/en?keywords=486-3382-ND))
- MOSFET to control LEDs
- circuit board to mount things to (I used a [Quarter-sized Perma-Proto](https://www.adafruit.com/product/589))
- 12 volt LED strip, I had some spare Armacost 120 LEDs/meter hanging around. (I used three 180
  mm strips lined up next to each other at the end of the arm.)
- zip-lock bag of sand or something to give the base some weight




-----

# Notes

  - ATTiny default clock speed is so slow the lights flicker when dimmed so you'll want to up it
