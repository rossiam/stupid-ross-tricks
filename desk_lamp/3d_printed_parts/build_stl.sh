#!/bin/bash

for part in top bottom
do
	openscad -o stl/${part}.stl -D 'part="'${part}'"' lamp_base.scad
done
