part = "test"; // top, bottom, test, jack_mount, encoder_test, size_test, support_test

echo(part=part);

wt = 1.6;
chamfer_radius = 0.4;
// Need to "raise the floor" by a few mm to allow room for screws in bottom
raised_floor = 4 - wt;

jack_mount_width = 12.8 + 2 * wt + 12;
jack_mount_depth = 16.2 + 2 * wt;
jack_mount_height = jack_mount_width;

// need 12 mm height for weight + at least 2 mm for screws
base_height = jack_mount_height - wt + chamfer_radius;
echo(base_height=base_height);
arm_offset = 15;
t = 0.2;
knob_t = 0.1;

wt_t = wt + t;

id = 165;
od = id + 2 * wt;

iid = 52.4; // 52.4;

$fn = 180;

nozzle_width = 0.5;


if (part == "top") {
	rotate([180, 0, 0]) lamp_base_top();
} else if (part == "bottom") {
	lamp_base_bottom();
} else if (part == "knob") {
	knob();
} else if (part == "jack_mount") {
	jack_mount();
} else if (part == "tap_test") {
	intersection() {
		translate([0, 0, -50]) cylinder(d = iid + 5, h = 100);
		rotate([180, 0, 0]) lamp_base_top();
		translate([14, -8, -wt - 0.1]) cube([100, 16, 10 + wt + 0.1]);
	}
} else if (part == "screw_test") {
	translate([iid + 10, 0, wt]) intersection() {
		translate([0, 0, -50]) cylinder(d = iid + 5, h = 100);
		rotate([180, 0, 0]) lamp_base_top();
	}
	intersection() {
		lamp_base_bottom();
		translate([0, 0, -0.1]) cylinder(d = iid + 5, h = 7.72);
	}
} else if (part == "support_test") {
	intersection() {
		lamp_base_bottom();
		translate([0, 0, -0.1]) cylinder(d = iid + 5, h = 7.72);
		translate([14, -8, 0]) cube([100, 16, 100]);
	}
} else if (part == "size_test") {
	difference() {
		cylinder(d = od, h = 1);
		translate([0, 0, -0.1]) cylinder(d = id, h = 2.2);
	}
} else if (part == "encoder_test") {
	rotate([180, 0, 0]) intersection() {
		lamp_base_top();
		translate([-(12.5 + 2 * wt_t) / 2, -(13.2 + 2 * wt_t) / 2, -2]) cube([12.5 + 2 * wt_t, 13.2 + 2 * wt_t, 15]);
	}
} else if (part == "test") {
	translate([0, 0, base_height * 2 + 10]) lamp_base_top();
	lamp_base_bottom();
	translate([0, 0, base_height * 2 + 40]) rotate([180, 0, 0]) knob();
	translate([0, 0, base_height * 2 + 10 - 6.5]) encoder();

	translate([-od/2 + arm_offset - 10, -10, base_height * 2 + 10 + wt]) color("silver") {
		cube([60, 20, 255]);
		translate([-2.5, -2.5, 255]) cube([572, 25, 3.3]);
	}
}

module encoder() {
	translate([-12.5/2, -13.2/2, 0]) cube([12.5, 13.2, 6.5]);
	translate([0, 0, 6.4]) cylinder(h = 5.1, d = 7);
	translate([0, 0, 6.4 + 5]) difference() {
		cylinder(h = 10.1, d = 6);
		translate([-4, 1.5, 3]) cube([8, 4, 8]);
	}
}

// 8 mm high when depressed
module knob() difference() {
	chamfered_cylinder(h = 10, d = 20);
	translate([0, 0, 1.4]) difference() {
		cylinder(d = 6 + t, h = 10.1);
		translate([1.5 - t, -7, -0.1]) cube([6, 14, 2.1]);
	}
	translate([0, 0, 6]) cylinder(d = 13, h = 10);
}

module chamfered_box(size, chamfer_top = true) {
	cr2 = 2 * chamfer_radius;
	hull() {
		translate([chamfer_radius, chamfer_radius, 0]) cube([size[0]-cr2, size[1]-cr2,0.1]);
		if (chamfer_top) {
			translate([0, 0, chamfer_radius]) cube([size[0], size[1], size[2]-2*chamfer_radius]);
			translate([chamfer_radius, chamfer_radius, size[2]-0.1]) cube([size[0]-cr2, size[1]-cr2,0.1]);
		} else {
			translate([0, 0, chamfer_radius]) cube([size[0], size[1], size[2]-chamfer_radius]);
		}
	}
}

module chamfered_cylinder(d, h, top = true, bottom = true) hull() {
	if (bottom == false) {
		cylinder(d = d - 2 * chamfer_radius, h = h);
		cylinder(d = d, h = h - chamfer_radius);
	} else if (top == false) {
		cylinder(d = d - 2 * chamfer_radius, h = h);
		translate([0, 0, chamfer_radius]) cylinder(d = d, h = h - chamfer_radius);
	} else {
		cylinder(d = d - 2 * chamfer_radius, h = h);
		translate([0, 0, chamfer_radius]) cylinder(d = d, h = h - 2 * chamfer_radius);
	}
}

// Things at the location where the two parts are put together
module at_screw_locations() {
	for (angle = [58, 167, 193, 302]) rotate([0, 0, angle]) translate([(od-5.4)/2-2*wt-t, 0, 0]) children();
}

module lamp_base_top() color("orange") {
	// it's weird but for now the "bottom" of this part (z = 0) is wt below the very top
	difference() {
		union() {
			chamfered_cylinder(d = od, h = wt, bottom = false);
			translate([-(12.5 + 2 * wt_t) / 2, -(13.2 + 2 * wt_t) / 2, -2]) cube([12.5 + 2 * wt_t, 13.2 + 2 * wt_t, 2.1]);
			// extra support for arm
			translate([-od/2 + arm_offset - 10, -10, -wt * 2]) difference() {
				translate([-wt, 0, 0]) cube([60 + wt, 20, wt * 2 + 0.1]);
				translate([wt, wt, -0.1]) cube([60 - wt * 2, 20 - wt * 2, wt * 2 + 0.3]);
			}
			translate([-19.6, -16, -wt * 2]) cube([5, 32, wt * 2 + 0.1]);
			// holes for mounting t-slot arm
			for (x = [0:20:40]) translate([-od/2 + arm_offset + x, 0, -wt * 2]) cylinder(d = 10, h = wt * 2 + 0.1);
		}

		// holes for mounting t-slot arm
		for (x = [0:20:40]) translate([-od/2 + arm_offset + x, 0, -wt * 2 - 0.1]) cylinder(d = 5.4, h = wt * 3 + 0.3);
		// hole for wire going up t-slot to LEDs
		translate([-od / 2 + arm_offset + 30, 0, - wt * 2 - 0.1]) cylinder(d = 11, h = wt * 3 + 0.3);
		// mount for encoder
		translate([0, 0, -0.1]) cylinder(d = 7.8, h = wt + 0.2);
		translate([-(12.5 + 2 * t) / 2, -(13.2 + 2 * t) / 2, -2.1]) cube([12.5 + 2 * t, 13.2 + 2 * t, 2.1]);
	}

	// lip slotting into bottom
	translate([0, 0, -wt * 2]) difference() {
		cylinder(d = id - 2 * t, h = wt * 2 + 0.1);
		translate([0, 0, -0.1]) cylinder(d = id - 2 * t - 2 * wt, h = wt * 2 + 0.3);
	}

	// standoffs for screws from bottom to hold to bottom
	standoff_height = base_height - 3 * wt - 2 * t - raised_floor - 2;
	at_screw_locations() {
		translate([0, 0, -standoff_height]) difference() {
			chamfered_cylinder(d = 5.4 + 2 * wt, h = standoff_height, top = false);

			translate([0, 0, -0.1]) cylinder(d = 4.4, h = standoff_height - wt + 0.1);
		}
	}
}

module jack_mount() {
	difference() {
		chamfered_box([jack_mount_width, jack_mount_depth, jack_mount_height]);
		translate([wt, -0.1, wt]) chamfered_box([jack_mount_width - wt * 2, jack_mount_depth - wt + 0.1, jack_mount_height - 2 * wt]);
		translate([jack_mount_width / 2, jack_mount_depth, jack_mount_height / 2]) rotate([90, 90, 0]) translate([0, 0, -0.1]) intersection() {
			cylinder(d = 12.8 + t, h = wt + 0.2);
			translate([-(12.8 + t) / 2, -(11.8 + t) / 2, 0]) cube([12.8 + t, 11.8 + t, wt + 0.2]);
		}
	}
}

module lamp_base_bottom() color("brown") {
	difference() {
		union() {
			difference() {
				union() {
					chamfered_cylinder(d = od, h = base_height - wt, top = false);
					translate([-od/2 + 1.5 * wt, -jack_mount_width / 2, 0]) rotate([0, 0, 90]) jack_mount();
				}
				translate([0, 0, wt]) cylinder(d = id, h = base_height + 0.1);
				// hole into to jack mount
				translate([-od / 2 - wt - 1, -jack_mount_width / 2 + wt, wt]) cube([20, jack_mount_width - 2 * wt, base_height - 2 * wt + 0.1]);
				// cut away top of jack mount for lid
				translate([0, 0, base_height - wt]) cylinder(d = od, h = base_height);
			}
			// circle ridge inside weight
			difference() {
				translate([0, 0, 0.1]) chamfered_cylinder(d = iid, h = wt * 2 + raised_floor + 2 - 0.1, bottom = false);
				cylinder(d = iid - 2 * wt, h = wt * 2 + raised_floor + 2 + 0.2);
			}

			at_screw_locations() translate([0, 0, 0.1]) {
				// standoffs for screws from bottom to hold top on
				chamfered_cylinder(d = 5.4 + 4 * wt, h = wt + raised_floor - 0.1, bottom = false);
				chamfered_cylinder(d = 5.4 + 2 * wt, h = 2 * wt + raised_floor - 0.1 + 2, bottom = false);
				// raise lines
//				translate([5.4 / 2, -wt/2, 0]) chamfered_box([(od - iid) / 2 + wt, wt, raised_floor + wt - 0.1]);
			}
			for (angle = [45 : 90 : 315]) rotate([0, 0, angle]) translate([iid/2-wt/2, -wt/2, 0]) chamfered_box([(od - iid) / 2, wt, raised_floor + wt - 0.1]);
		}
		// holes for screws from bottom to hold top on
		at_screw_locations() translate([0, 0, -0.1]) difference() {
			union() {
				cylinder(d = 5.4, h = 2 * wt + raised_floor + 2 + 0.2);
				cylinder(d = 10, h = wt + 0.2);
			}

			// support structure for screw holes
			difference() {
				translate([0, 0, 0.1]) cylinder(d = 5.4 + nozzle_width * 3, h = wt - 0.1);
				cylinder(d = 5.4 - nozzle_width, h = 2 * wt + raised_floor + 2 + 0.2);
			}
		}
	}
}
